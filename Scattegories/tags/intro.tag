<intro>

	<div class="intro">Complete each category using words that begin with the given letter.</div>
	<h3>Choose your letter</h3>
	<button onclick={ chooseLetter }>Choose Letter</button>

	<div class="table">
		<div class= "container">
			<div class="row">
				<div class="col-md-2">
					<h5>Animal</h5>
					<input id="animalinput" type="text" placeholder="Enter animal">
					</div>
				<div class="col-md-2">
					<h5>Adjective</h5>
					<input id="adjectiveinput" type="text" placeholder="Enter adjective">
				</div>
				<div class="col-md-2">
					<h5>Animal</h5>
					<input id="nouninput" type="text" placeholder="Enter noun">
				</div>
				<div class="col-md-2">
					<h5>Verb</h5>
					<input id="verbinput" type="text" placeholder="Enter verb">
				</div>
				<div class="col-md-2">
					<h5>Brand</h5>
					<input id="brandinput" type="text" placeholder="Enter brand">
				</div>
				<div class="col-md-2">
					<h5>Celebrity</h5>
					<input id="celebrityinput" type="text" placeholder="Enter celebrity">
				</div>
			</div>
		</div>
		<button onclick={ createTable }>STOP!</button>
		<!-- <table each={ table }></table> -->
	</div>


	<script>
		
		//console.log("WORKING, YEAH!")

		//this.table = this.opts.table;

		//this.createTable = function(event) {
			//var tableObj {
				//animal: this.animalinput.value,
				//adjective: this.adjectiveinput.value,
				//noun: this.nouninput.value,
				//verb: this.verbinput.value,
				//brand: this.brandinput.value,
				//celebrity: this.celebrityinput.value
			//};

			//this.table.push(tableObj);

			//this.animalinput.value = "";
			//this.adjectiveinput.value = "";
			//this.nouninput.value = "";
			//this.verbinput.value = "";
			//this.brandinput.value = "";
			//this.celebrityinput.value = "";
		//};

	</script>

</intro>