Parse.initialize ("JOiRQ4zeHDhhGbzr4oKw9QmKyrlpTcIH6k6uNzXY", "pnXpWoBz1Vx65MB38SNOVjcHnnQYG6L0jEpnVisJ")

var data = {
	animal: "This is an animal",
	adjective: "This is an adjective",
	noun: "This is a noun",
	verb: "This is a verb",
	brand: "This is a brand",
	celebrity: "This is a celebrity",
	};

var table = [
{
	animal: "Ant",
	adjective: "Amusing",
	noun: "Alley",
	verb: "Affect",
	brand: "ABC",
	celebrity: "Ariana Grande",
},
{
	animal: "Bull",
	adjective: "Boring",
	noun: "Bullet",
	verb: "Bond",
	brand: "BMW",
	celebrity: "Bruno Mars",
},
];

riot.mount('intro', {
	table: table,
});